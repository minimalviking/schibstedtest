package com.schibsted.android.chatbot;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

public class UserManager {
	private static final String USER_MANAGER = "userManager";
	private static final String CURRENT_USER = "currentUser";

	private static UserManager instance = new UserManager();

	public static UserManager getInstance() {
		return instance;
	}

	String currentUser;

	private UserManager() {
		SharedPreferences prefs = ChatApp.getContext().getSharedPreferences(USER_MANAGER, Context.MODE_PRIVATE);
		currentUser = prefs.getString(CURRENT_USER, null);
	}

	public boolean isUserLoggedIn() {
		return currentUser != null;
	}

	public void logOut(Context context) {
		setCurrentUser(null);
		Navigator.loginScreen(context);
	}

	public void setCurrentUser(@Nullable String username) {
		SharedPreferences prefs = ChatApp.getContext().getSharedPreferences(USER_MANAGER, Context.MODE_PRIVATE);
		prefs.edit().putString(CURRENT_USER, username).apply();
		currentUser = username;
	}

	public String getCurrentUser() {
		return currentUser;
	}
}
