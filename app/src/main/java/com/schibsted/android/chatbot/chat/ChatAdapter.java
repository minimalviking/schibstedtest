package com.schibsted.android.chatbot.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.UserManager;
import com.schibsted.android.chatbot.api.models.Chat;
import com.squareup.picasso.Picasso;

import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final List<Chat> items;
	private final Context context;

	private static final int USER_MSG = 0;
	private static final int OTHER_MSG = 1;

	public ChatAdapter(List<Chat> items, Context context) {
		this.items = items;
		this.context = context;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		Context context = parent.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);
		RecyclerView.ViewHolder viewHolder;
		View contactView;
		if (viewType == OTHER_MSG) {
			contactView = inflater.inflate(R.layout.row_chat, parent, false);
			viewHolder = new ViewHolderOther(contactView);

		} else {
			contactView = inflater.inflate(R.layout.row_chat_user, parent, false);
			viewHolder = new ViewHolderUser(contactView);
		}
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		if (holder.getItemViewType() == USER_MSG) {
			bindViewHolderUser((ViewHolderUser) holder, position);
		} else {
			bindViewHolderOther((ViewHolderOther) holder, position);
		}
	}

	private void bindViewHolderUser(ViewHolderUser holder, int position) {
		Chat chat = items.get(position);
		holder.message.setText(chat.getContent());
		holder.time.setText(chat.getTime());
	}

	private void bindViewHolderOther(ViewHolderOther holder, int position) {
		Chat chat = items.get(position);
		holder.message.setText(chat.getContent());
		Picasso.with(context)
			.load(chat.getUserImageUrl())
			.transform(new CropCircleTransformation())
			.into(holder.photo);
		holder.info.setText(chat.getUsername() + " - " + chat.getTime().replaceAll("[^\\d:]", ""));
	}

	@Override
	public int getItemViewType(int position) {
		Chat chat = items.get(position);
		if (chat.getUsername().equals(UserManager.getInstance().getCurrentUser())) {
			return USER_MSG;
		} else {
			return OTHER_MSG;
		}
	}

	public List<Chat> getItems() {
		return items;
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	static class ViewHolderOther extends RecyclerView.ViewHolder {
		private TextView message;
		private TextView info;
		private ImageView photo;

		public ViewHolderOther(View itemView) {
			super(itemView);
			message = (TextView) itemView.findViewById(R.id.row_message);
			info = (TextView) itemView.findViewById(R.id.row_info);
			photo = (ImageView) itemView.findViewById(R.id.row_photo);
		}
	}

	static class ViewHolderUser extends RecyclerView.ViewHolder {
		private TextView message;
		private TextView time;

		public ViewHolderUser(View itemView) {
			super(itemView);
			message = (TextView) itemView.findViewById(R.id.row_message);
			time = (TextView) itemView.findViewById(R.id.row_time);
		}
	}
}
