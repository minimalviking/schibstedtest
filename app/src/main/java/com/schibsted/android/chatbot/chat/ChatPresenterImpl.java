package com.schibsted.android.chatbot.chat;

import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.UserManager;
import com.schibsted.android.chatbot.api.RetrofitApi;
import com.schibsted.android.chatbot.api.models.Chat;
import com.schibsted.android.chatbot.api.models.Chats;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatPresenterImpl implements ChatPresenter {
	private final ChatView view;

	public ChatPresenterImpl(ChatView view) {
		this.view = view;
	}

	@Override
	public void onLoad() {
		view.showLoading(true);
		RetrofitApi.getInstance().getApi().getChats().enqueue(new Callback<Chats>() {
			@Override
			public void onResponse(Call<Chats> call, Response<Chats> response) {
				view.showLoading(false);
				view.showMessages(response.body().getChats());
			}

			@Override
			public void onFailure(Call<Chats> call, Throwable t) {
				view.showLoading(false);
				view.showNetworkError(R.string.error_chat_retrieval);
			}
		});
	}

	@Override
	public void onMessengSent(String msg) {
		if (msg.length() == 0) {
			return;
		}

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		String time = df.format(c.getTime());

		Chat chat = new Chat(UserManager.getInstance().getCurrentUser(), msg, time);
		view.addMessage(chat);
	}

	@Override
	public void onRecreated(Chats chats) {
		view.showMessages(chats.getChats());
	}
}
