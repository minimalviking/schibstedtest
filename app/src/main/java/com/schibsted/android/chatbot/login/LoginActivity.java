package com.schibsted.android.chatbot.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.schibsted.android.chatbot.Navigator;
import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.UserManager;

import static com.schibsted.android.chatbot.Navigator.chatScreen;

/**
 * A login screen that offers login via Name & Surname.
 */
public class LoginActivity extends AppCompatActivity {

	private UsernameValidator usernameValidator;

	// UI references.
    private EditText mName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    if (UserManager.getInstance().isUserLoggedIn()) {
		    Navigator.chatScreen(this);
		    finish();
	    }

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mName = (EditText) findViewById(R.id.name);
        Button mNameSignInButton = (Button) findViewById(R.id.sign_in_button);
        mNameSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

	    usernameValidator = new UsernameValidator(this);
    }


    /**
     * Attempts to sign in with Name & Surname.
     * If there are form errors (invalid name, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mName.setError(null);

        // Store values at the time of the login attempt.
        String name = mName.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(name)) {
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
        } else if (!isNameValid(name)) {
            mName.setError(getString(R.string.error_invalid_name));
            focusView = mName;
            cancel = true;
        }

        if (cancel) {
            // There was an error: don't attempt login and focus the first form field with an error.
            focusView.requestFocus();
        } else {
	        //save new username to UserManager
	        UserManager.getInstance().setCurrentUser(name);
            // Start the new activity
	        chatScreen(this);
        }
    }

    private boolean isNameValid(String name) {
	    return usernameValidator.isValid(name);
    }
}

