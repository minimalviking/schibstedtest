package com.schibsted.android.chatbot.login;

import android.content.Context;

import com.schibsted.android.chatbot.R;

import java.util.ArrayList;
import java.util.Arrays;

public class UsernameValidator {

	private final ArrayList<String> forbiddenNames;

	public UsernameValidator(Context context) {
		String[] array = context.getResources().getStringArray(R.array.forbidden_names);
		forbiddenNames = new ArrayList<>(Arrays.asList(array));
	}

	public boolean isValid(String name) {
		return !forbiddenNames.contains(name.toLowerCase());
	}
}
