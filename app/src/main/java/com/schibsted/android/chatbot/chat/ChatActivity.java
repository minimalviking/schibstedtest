package com.schibsted.android.chatbot.chat;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.UserManager;
import com.schibsted.android.chatbot.api.models.Chat;
import com.schibsted.android.chatbot.api.models.Chats;

import org.parceler.Parcels;

import java.util.List;

public class ChatActivity extends AppCompatActivity implements ChatView {

	private ChatPresenter presenter;
	private ProgressBar progressBar;
	private RecyclerView recyclerView;
	private View root;
	private ChatAdapter adapter;
	private EditText entryText;

	private final static String PARCEL_KEY = "chats";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		initViews();

		presenter = new ChatPresenterImpl(this);
		if (savedInstanceState!=null && savedInstanceState.containsKey(PARCEL_KEY)) {
			presenter.onRecreated((Chats) Parcels.unwrap(savedInstanceState.getParcelable(PARCEL_KEY)));
		} else {
			presenter.onLoad();
		}

    }

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(PARCEL_KEY, Parcels.wrap(new Chats(adapter.getItems())));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_chat, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.logout) {
			UserManager.getInstance().logOut(this);
			finish();
			return true;
		}

		return false;
	}

	private void initViews() {
		// Set the title on screen
		setTitle(getString(R.string.chat_title) + UserManager.getInstance().getCurrentUser());

		progressBar = (ProgressBar) findViewById(R.id.chat_progress);
		recyclerView = (RecyclerView) findViewById(R.id.chat_recyclerview);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		root = findViewById(R.id.chat_root);
		Button send = (Button) findViewById(R.id.chat_send);
		entryText = (EditText) findViewById(R.id.chat_text_entry);

		send.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String msg = entryText.getText().toString();
				presenter.onMessengSent(msg);
			}
		});
	}

	@Override
	public void showLoading(boolean show) {
		if (show) {
			progressBar.setVisibility(View.VISIBLE);
		} else {
			progressBar.setVisibility(View.GONE);
		}
	}

	@Override
	public void showNetworkError(@StringRes int msg) {
		Snackbar snackbar = Snackbar
			.make(root, msg, Snackbar.LENGTH_INDEFINITE)
			.setAction(R.string.retry, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					presenter.onLoad();
				}
			});
		snackbar.show();
	}

	@Override
	public void showMessages(List<Chat> chats) {
		adapter = new ChatAdapter(chats, this);
		recyclerView.setAdapter(adapter);
		recyclerView.scrollToPosition(adapter.getItemCount()-1);
	}

	@Override
	public void addMessage(Chat chat) {
		adapter.getItems().add(chat);
		adapter.notifyItemInserted(adapter.getItemCount()-1);
		recyclerView.smoothScrollToPosition(adapter.getItemCount()-1);
		entryText.setText("");
	}
}
