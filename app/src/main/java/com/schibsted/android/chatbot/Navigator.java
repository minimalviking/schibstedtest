package com.schibsted.android.chatbot;

import android.content.Context;
import android.content.Intent;

import com.schibsted.android.chatbot.chat.ChatActivity;
import com.schibsted.android.chatbot.login.LoginActivity;

public class Navigator {

	public static void loginScreen(Context context) {
		Intent intent = new Intent(context, LoginActivity.class);
		context.startActivity(intent);
	}

	public static void chatScreen(Context context) {
		Intent intent = new Intent(context, ChatActivity.class);
		context.startActivity(intent);
	}
}
