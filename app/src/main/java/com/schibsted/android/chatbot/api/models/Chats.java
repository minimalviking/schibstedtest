package com.schibsted.android.chatbot.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Chats {

	@SerializedName("chats")
	@Expose
	List<Chat> chats = null;

	public Chats() {
	}

	public Chats(List<Chat> chats) {
		this.chats = chats;
	}

	public List<Chat> getChats() {
		return chats;
	}

	public void setChats(List<Chat> chats) {
		this.chats = chats;
	}

}
