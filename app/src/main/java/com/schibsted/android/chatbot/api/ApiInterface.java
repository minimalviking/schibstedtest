package com.schibsted.android.chatbot.api;

import com.schibsted.android.chatbot.api.models.Chats;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
	@GET("chat.json")
	Call<Chats> getChats();

}
