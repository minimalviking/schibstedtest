package com.schibsted.android.chatbot.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi {
	private static RetrofitApi instance = new RetrofitApi();
	private final ApiInterface api;

	public static RetrofitApi getInstance() {
		return instance;
	}

	private RetrofitApi() {
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl("https://s3-eu-west-1.amazonaws.com/rocket-interview/")
			.addConverterFactory(GsonConverterFactory.create())
			.build();
		api = retrofit.create(ApiInterface.class);
	}

	public ApiInterface getApi() {
		return api;
	}
}
