package com.schibsted.android.chatbot;

import android.app.Application;

public class ChatApp extends Application{
	private static Application context;

	@Override
	public void onCreate() {
		super.onCreate();
		context = this;
	}

	public static Application getContext() {
		return context;
	}
}
