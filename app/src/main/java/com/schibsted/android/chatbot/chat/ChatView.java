package com.schibsted.android.chatbot.chat;

import android.support.annotation.StringRes;

import com.schibsted.android.chatbot.api.models.Chat;

import java.util.List;

public interface ChatView {
	void showLoading(boolean show);

	void showNetworkError(@StringRes int error_chat_retrieval);

	void showMessages(List<Chat> chats);

	void addMessage(Chat chat);
}
