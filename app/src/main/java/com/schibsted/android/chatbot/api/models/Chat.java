package com.schibsted.android.chatbot.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Chat {

	@SerializedName("username")
	@Expose
	String username;
	@SerializedName("content")
	@Expose
	String content;
	@SerializedName("userImage_url")
	@Expose
	String userImageUrl;
	@SerializedName("time")
	@Expose
	String time;

	public Chat() {
	}

	public Chat(String username, String content, String time) {
		this.username = username;
		this.content = content;
		this.time = time;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}