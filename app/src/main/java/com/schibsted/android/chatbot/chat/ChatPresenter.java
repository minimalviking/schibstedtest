package com.schibsted.android.chatbot.chat;

import com.schibsted.android.chatbot.api.models.Chats;

public interface ChatPresenter {
	void onLoad();

	void onMessengSent(String msg);

	void onRecreated(Chats chats);
}
